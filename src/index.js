/**
 * @module custom-fetch
 */

/**
 * @callback fetchHandler The function that actually handles the fetch request
 * @argument {Request} request
 * @argument {Signal?} signal
 * @async
 * @returns {Response|string}
 */

/**
 * @callback Request
 * @class
 * @argument {string} url 
 * @argument {object}	options
 */

/**
 * @callback Response
 * @class
 * @argument {string} body 
 * @argument {object}	options
 */

/**
 * @class Custom Fetch constructor
 * @param {fetchHandler} fetchHandler Callback for handling fetch request.
 * @param {object} fetchConstructors Object containing constructors for creating fetch Request and Response classes
 * @param {Request} fetchConstructors.Request Constructor for a fetch Request object
 * @param {Response} fetchConstructors.Response Constructor for a fetch Response object
 */
export default function CustomFetch(fetchHandler, { Request, Response } = globalThis){
	return Object.setPrototypeOf(
		async (url, options) => {
			const response = await fetchHandler.call(this, new Request(url, options), options?.signal);
			if(response instanceof Response) {
				return response;
			}
			return new Response(response);
		},
		new.target.prototype
	);
}

CustomFetch.prototype = Function.prototype;
