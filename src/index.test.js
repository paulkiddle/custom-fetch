import CustomFetch from './index.js';
import { jest } from '@jest/globals';

test('Abstract fetch', async ()=>{
	class MockCls {
		constructor(...args) {
			this.args = args;
		}
	}
	let rtn = 'value';
	const cb = jest.fn(()=>rtn);

	Object.assign(globalThis, { Request: MockCls, Response: MockCls });
	const fetch = new CustomFetch(cb);

	const v1 = await fetch('test');
	expect(cb).toHaveBeenCalledWith(new MockCls('test', undefined), undefined);
	expect(v1).toEqual(new MockCls(rtn));

	rtn = new MockCls('return val');
	const opts ={ signal: 'signal' };
	const v2 = await fetch('test', opts);
	expect(cb).toHaveBeenCalledWith(new MockCls('test', opts), opts.signal);
	expect(v2).toBe(rtn);
});
