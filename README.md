# Custom Fetch

Creates a function with the same API as WHATWG's `fetch`,
but with your own custom behaviour. Ideal for testing or
for modifying an HTTP request before sending.

```javascript
import CustomFetch from 'custom-fetch';
import * as NodeFetch from 'node-fecth';

// You need to bring your own Request and Response constructors
// There are already libraries that implement these and I didn't want
// to re-implement them just for the sake of it.
const { Request, Response } = NodeFetch;

const fetch = new CustomFetch(async (request, signal) => {
    // `request` is an instance of the provided Request class,
    // constructed based on the arguments passed to `fetch`.
    // `signal` is the value of `signal` on the fetch options object
    // (or undefined if not provided)

    switch(request.url) {
        case 'http://horses.example/':
            // You can return just a response body and it will be
            // used as the first argument to the Response constructor
            return 'Welcome to horses.example';
        case 'http://flowers.example/foxglove.json':
            // You can also return a preconstructed Response object
            // (as long as it's of the same class as the given
            // Response constructor)
            return new Response(
                JSON.stringify({
                    name: 'Foxglove',
                    scientificName: 'Digitalis purpurea',
                    gbifId: 5414995
                }),
                {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
        default:
            // Since NodeFetch.fetch returns a Response object,
            // we can just forward the request on if we like,
            // or we could make modifications to the request
            // before doing so.
            request = new Request(request, { headers: { 'X-Favourite-Color': 'blue' } })
            return NodeFetch.fetch(request, { signal });
},
// Let CustomFetch know how to construct the request and response objects
{ Request, Response }
);

export default fetch('http://horses.example/');
```

## Dependencies
None
<a name="module_custom-fetch"></a>

## custom-fetch

* [custom-fetch](#module_custom-fetch)
    * [module.exports](#exp_module_custom-fetch--module.exports) ⏏
        * [new module.exports(fetchHandler, fetchConstructors)](#new_module_custom-fetch--module.exports_new)
        * [~Request](#module_custom-fetch--module.exports..Request) : <code>function</code>
            * [new Request(url, options)](#new_module_custom-fetch--module.exports..Request_new)
        * [~Response](#module_custom-fetch--module.exports..Response) : <code>function</code>
            * [new Response(body, options)](#new_module_custom-fetch--module.exports..Response_new)
        * [~fetchHandler](#module_custom-fetch--module.exports..fetchHandler) ⇒ <code>Response</code> \| <code>string</code>

<a name="exp_module_custom-fetch--module.exports"></a>

### module.exports ⏏
Custom Fetch constructor

**Kind**: Exported class  
<a name="new_module_custom-fetch--module.exports_new"></a>

#### new module.exports(fetchHandler, fetchConstructors)

| Param | Type | Description |
| --- | --- | --- |
| fetchHandler | <code>fetchHandler</code> | Callback for handling fetch request. |
| fetchConstructors | <code>object</code> | Object containing constructors for creating fetch Request and Response classes |
| fetchConstructors.Request | <code>Request</code> | Constructor for a fetch Request object |
| fetchConstructors.Response | <code>Response</code> | Constructor for a fetch Response object |

<a name="module_custom-fetch--module.exports..Request"></a>

#### module.exports~Request : <code>function</code>
**Kind**: inner class of [<code>module.exports</code>](#exp_module_custom-fetch--module.exports)  
<a name="new_module_custom-fetch--module.exports..Request_new"></a>

##### new Request(url, options)

| Param | Type |
| --- | --- |
| url | <code>string</code> | 
| options | <code>object</code> | 

<a name="module_custom-fetch--module.exports..Response"></a>

#### module.exports~Response : <code>function</code>
**Kind**: inner class of [<code>module.exports</code>](#exp_module_custom-fetch--module.exports)  
<a name="new_module_custom-fetch--module.exports..Response_new"></a>

##### new Response(body, options)

| Param | Type |
| --- | --- |
| body | <code>string</code> | 
| options | <code>object</code> | 

<a name="module_custom-fetch--module.exports..fetchHandler"></a>

#### module.exports~fetchHandler ⇒ <code>Response</code> \| <code>string</code>
**Kind**: inner typedef of [<code>module.exports</code>](#exp_module_custom-fetch--module.exports)  

| Param | Type |
| --- | --- |
| request | <code>Request</code> | 
| signal | <code>Signal</code> | 

